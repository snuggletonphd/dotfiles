" =============================================================================
" Filename: autoload/lightline/colorscheme/powerline.vim
" Author: itchyny
" License: MIT License
" Last Change: 2013/09/07 15:54:41.
" =============================================================================


let s:foreground = '#dedbeb'
let s:background = '#2a2331'

let s:grayd = '#28222d'
let s:grayl = '#302838'

let s:redd = '#ed3f7f'
let s:redl = '#fb5c8e'

let s:greend = '#a2baa8'
let s:greenl = '#bfd1c3'

let s:yellowd = '#eacac0'
let s:yellowl = '#f0ddd8'

let s:purpled = '#9985d1'
let s:purplel = '#b4a4de'

let s:pinkd = '#e68ac1'
let s:pinkl = '#edabd2'

let s:cyand = '#aabae7'
let s:cyanl = '#c4d1f5'

let s:whited = '#dedbeb'
let s:whitel = '#edebf7'
let s:comment = '#634e75'



let s:p                 =   {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left     =   [ [s:whitel, s:purplel, 'bold'], [s:whitel, s:purpled], [ s:whitel, s:redd, 'bold' ] ]
let s:p.normal.middle   =   [ [ s:whited, s:background ] ]
let s:p.normal.right    =   [ [s:whitel, s:purpled], [s:whitel, s:purplel], [s:purplel, s:purpled] ]
let s:p.normal.error    =   [ [ s:pinkl, s:redl ] ]
let s:p.normal.warning  =   [ [ s:pinkl, s:yellowd ] ]

let s:p.insert.left     =   [ [s:whited, s:redl, 'bold'], [s:whited, s:redd], [ s:whitel, s:redd, 'bold' ] ]
let s:p.insert.middle   =   [ [ s:whited, s:background ] ]
let s:p.insert.right    =   [ [ s:whited, s:redd ], [ s:whited, s:redl ], [ s:whited, s:redl ] ]

let s:p.visual.left     =   [ [s:whited, s:greenl, 'bold'], [s:whited, s:greend], [ s:whited, s:redd ] ]
let s:p.visual.middle   =   [ [s:whited, s:background ] ]
let s:p.visual.right    =   [ [ s:whited, s:greend ], [ s:whited, s:greenl ] ]

let s:p.inactive.right  =   [ [s:yellowl, s:cyanl], [s:yellowl, s:cyanl], [s:yellowl, s:cyanl] ]
let s:p.inactive.left   =   s:p.inactive.right[1:]

let s:p.replace.left    =   [ [s:whited, s:redl, 'bold'], [s:whited, s:cyanl] ]
let s:p.replace.middle  =   s:p.normal.middle
let s:p.replace.right   =   s:p.normal.right

let s:p.tabline.left    =   [ [ s:cyanl, s:cyand ] ]
let s:p.tabline.middle  =   [ [ s:cyanl, s:cyand ] ]
let s:p.tabline.right   =   [ [ s:cyanl, s:cyand ] ]
let s:p.tabline.tabsel  =   [ [ s:cyanl, s:cyand ] ]

let g:lightline#colorscheme#amora#palette = lightline#colorscheme#fill(s:p)
