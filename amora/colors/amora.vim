" Maintainer: fuckin' me

set background=dark
hi clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name='amora'

hi Normal guifg=#dedbeb ctermfg=254 guibg=#2a2331 ctermbg=235 gui=NONE cterm=NONE
hi Comment guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Constant guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi String guifg=#a2baa8 ctermfg=145 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Character guifg=#edabd2 ctermfg=218 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Number guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Boolean guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Float guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Identifier guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Function guifg=#9985d1 ctermfg=104 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi Statement guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Conditional guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Repeat guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Label guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Operator guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Keyword guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi Exception guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi PreProc guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Include guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Define guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Title guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Macro guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreCondit guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Type guifg=#aabae7 ctermfg=146 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi StorageClass guifg=#aabae7 ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Structure guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Typedef guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Special guifg=#e68ac1 ctermfg=175 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpecialComment guifg=#aabae7 ctermfg=146 guibg=NONE ctermbg=NONE gui=italic cterm=italic
hi MoreMsg guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Error guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Todo guifg=#28222d ctermfg=235 guibg=#aabae7 ctermbg=146 gui=bold cterm=bold
hi Underlined guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Cursor guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi ColorColumn guifg=#141414 ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorLineNr guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SignColumn guifg=#141414 ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Conceal guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorColumn guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorLine guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Directory guifg=#fb5c8e ctermfg=204 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi DiffAdd guifg=#a2baa8 ctermfg=145 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffChange guifg=#a2baa8 ctermfg=145 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffDelete guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffText guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi ErrorMsg guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi VertSplit guifg=#141414 ctermfg=233 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Folded guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FoldColumn guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi IncSearch guifg=#28222d ctermfg=235 guibg=#fb5c8e ctermbg=204 gui=bold cterm=bold
hi LineNr guifg=#634e75 ctermfg=60 guibg=#211c26 ctermbg=235 gui=NONE cterm=NONE
hi MatchParen guifg=#bfd1c3 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NonText guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Pmenu guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PmenuSel guifg=#dedbeb ctermfg=254 guibg=#ed3f7f ctermbg=204 gui=bold cterm=bold
hi PmenuSbar guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PmenuThumb guifg=#634e75 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Question guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi QuickFixLine guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Search guifg=#28222d ctermfg=235 guibg=#fb5c8e ctermbg=204 gui=bold cterm=bold
hi SpecialKey guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellBad guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellCap guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellLocal guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellRare guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLine guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLineNC guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLineTerm guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLineTermNC guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLine guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLineSel guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLineFill guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Terminal guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Visual guifg=#dedbeb ctermfg=254 guibg=#b4a4de ctermbg=146 gui=bold cterm=bold
hi VisualNOS guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi WarningMsg guifg=#28222d ctermfg=235 guibg=#fb5c8e ctermbg=204 gui=bold cterm=bold
hi WildMenu guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi EndOfBuffer guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSError guifg=#ed3f7f ctermfg=204 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSPunctDelimiter guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSPunctBracket guifg=#dedbeb ctermfg=254 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSStringEscape guifg=#bfd1c3 ctermfg=251 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSParameter guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSField guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SVariable guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSVariableBuiltin guifg=#b4a4de ctermfg=146 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
