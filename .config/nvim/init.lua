require('snug')
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
-- local uv = vim.uv or vim.loop

-- Auto-install lazy.nvim if not present
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  print('Installing lazy.nvim....')
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
  print('Done.')
end

vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
	{'folke/tokyonight.nvim'},
    {'rose-pine/neovim', name = 'rose-pine'},
	{'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
	{'williamboman/mason.nvim'},
	{'williamboman/mason-lspconfig.nvim'},
	{'neovim/nvim-lspconfig'},
	{'hrsh7th/cmp-nvim-lsp'},
	{'hrsh7th/cmp-buffer'},
	{'hrsh7th/cmp-path'},
	{'hrsh7th/cmp-cmdline'},
	{'L3MON4D3/LuaSnip',
        version = 'v2.*',
        build = 'make install_jsregexp'
    },
	{'hrsh7th/nvim-cmp',},
	{'saadparwaiz1/cmp_luasnip'},
    {'windwp/nvim-autopairs',
    event = 'InsertEnter',
    config = true
    },
    {'machakann/vim-highlightedyank'},
    {'rstacruz/vim-closer'},
    {'nvim-tree/nvim-web-devicons'},
    {'stevearc/oil.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' }
    },
    {'tpope/vim-surround'},
    {'tpope/vim-commentary'},
    {'folke/zen-mode.nvim'},
    {'nvim-lua/plenary.nvim'},
    {'ThePrimeagen/harpoon',
        branch = 'harpoon2',
        dependencies = {'nvim-lua/plenary.nvim'},
        config = function()
            local harpoon = require('harpoon')
            harpoon:setup()

            vim.keymap.set('n', '<Leader>a', function()
                harpoon:list():add()
            end)
            vim.keymap.set('n', '<C-e>', function()
                harpoon.ui:toggle_quick_menu(harpoon:list())
            end)
            vim.keymap.set('n', '<A-a>', function()
                harpoon:list():select(1)
            end)
            vim.keymap.set('n', '<A-s>', function()
                harpoon:list():select(2)
            end)
            vim.keymap.set('n', '<A-d>', function()
                harpoon:list():select(3)
            end)
            vim.keymap.set('n', '<A-f>', function()
                harpoon:list():select(4)
            end)
            vim.keymap.set('n', '<C-S-P>', function()
                harpoon:list():prev()
            end)
            vim.keymap.set('n', '<C-S-N>', function()
                harpoon:list():next()
            end)
        end,
    },

    {'nvim-telescope/telescope.nvim', tag = '0.1.6',
    dependencies = { 'nvim-lua/plenary.nvim' } },
    -- treesitter config
    {'nvim-treesitter/nvim-treesitter',
    build = "TSUpdate",
    config = function ()
        local configs = require("nvim-treesitter.configs")

        configs.setup({
            ensure_installed = { "c", "lua", "vim" },
            sync_install = false,
            highlight = { enable = true },
            indent = { enable = true },
        })

    end
    }
})

-----------------------------------------------------
-- Telescope
-----------------------------------------------------

require('telescope').setup {
    defaults = {
        mappings = {
            i = {
            ["<C-h>"] = "which_key"
            }
        }
    },
        pickers = {},
        extensions = {}
}

-----------------------------------------------------
-- LSP Zero
-----------------------------------------------------

local lsp_zero = require('lsp-zero')

-- local opts = {noremap=true, silent=true}

lsp_zero.on_attach(function(client, bufnr)
	-- see :help lsp-zero-keybindings
	-- to learn available actions
	-- throw clangd binds in here??
	lsp_zero.default_keymaps({buffer = bufnr})
end)

lsp_zero.set_sign_icons({
	error = '󰰰',
	warn = '▲',
	hint = '',
	info = ''
})

lsp_zero.format_mapping('gq', {
	format_opts = {
		async = false,
		timeout_ms = 10000,
	},
	servers = {
		['clangd'] = {'c'},
	}
})

-----------------------------------------------------
-- Mason
-----------------------------------------------------

require('mason').setup({})
require('mason-lspconfig').setup({
    ensure_installed = {'clangd', 'lua_ls'},
	handlers = {
		function(server_name)
			require('lspconfig')[server_name].setup({})
		end,
	},
})

-----------------------------------------------------
-- Cmp
-----------------------------------------------------

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()
local luasnip = require('luasnip')

cmp.setup ({
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
            -- or require() another snippet engine
        end,
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        -- Ctrl+Space to trigger completion menu
        ['<C-Space>'] = cmp.mapping.complete(),
        -- Navigate between snippet placeholder
        ['<C-f>'] = cmp_action.luasnip_jump_forward(),
        ['<C-b>'] = cmp_action.luasnip_jump_backward(),
        -- Scroll up and down in the completion documentation
        ['<C-u>'] = cmp.mapping.scroll_docs(-4),
        ['<C-d>'] = cmp.mapping.scroll_docs(4),

        -- Testing hybrid binds between cmp selection and luasnip selection
        -- Enter key to confirm selection
        -- ['<CR>'] = cmp.mapping.confirm({select = false}),
        -- Tab to select items
		-- ['<Tab>'] = cmp.mapping.select_next_item({behavior = 'select'}),
		-- ['<S-Tab>'] = cmp.mapping.select_prev_item({behavior = 'select'}),

['<CR>'] = cmp.mapping(function(fallback)
        if cmp.visible() then
            if luasnip.expandable() then
                luasnip.expand()
            else
                cmp.confirm({
                    select = true,
                })
            end
        else
            fallback()
        end
    end),

    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.locally_jumpable(1) then
        luasnip.jump(1)
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
    }),
    sources = cmp.config.sources ({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
    }, {
        { name = 'buffer' },
    })
})

local ls = require("luasnip")
-- vim.keymap.set({"i"}, "<C-k>", function() ls.expand() end, {silent = false})
-- vim.keymap.set({"i", "s"}, "<C-l>", function() ls.jump(1) end, {silent = false})
-- vim.keymap.set({"i", "s"}, "<C-j>", function() ls.jump(-1) end, {silent = false})

vim.keymap.set({"i", "s"}, "<C-e>", function()
    if ls.choice_active() then
        ls.change_choice(1)
    end
end, {silent = false})

ls.setup ({
    snip_env = {
        s = function(...)
            local snip = ls.s(...)
            -- we can't just access the global 'ls_file_snippets', since it will be
            -- resolved in the environment of the scope in which it was defined.
            table.insert(getfenv(2).ls_file_snippets, snip)
    end,
    parse = function(...)
        local snip = ls.parser.parse_snippet(...)
        table.insert(getfenv(2).ls_file_snippets, snip)
    end,
    },
    enable_autosnippets = true,
    ls.add_snippets("all", {
        ls.snippet("r1", {
            ls.text_node("return 1;"),
        }),
        ls.snippet("r0", {
            ls.text_node("return 0;"),
        }),
        ls.snippet("r-", {
            ls.text_node("return -1;"),
        }),
    }, {
            type = "autosnippets",
            key = "all_auto",
        }),
})
require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/snippets"})
