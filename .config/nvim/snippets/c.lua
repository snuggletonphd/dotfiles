-- No need to define s, t etc. as they are globally defined by LuaSnip
-- args is a table, where 1 is the text in Placeholder 1, 2 the text in
-- placeholder 2,...
local function copy(args)
	return args[1]
end

return {

    s("mn", {
        t("int main(int argc, char** argv"),
        t({ ")", "{" }),
        i(0),
        t({"\t", ""}),
        t({"\treturn 0;", "}"}),
    }),

    s("rt",
    {
        t("return "),
        i(0),
        t(";"),
    }),

    -- if-else if
    s("ifi",
    {
        t("if ("),
        i(1, "condition"),
        t(") {"),
        t({"", "\t"}),
        i(2, ""),
        t({"", "} else if ("}),
        i(3, "condition 2"),
        t(") {"),
        t({"", "\t"}),
        i(0, ""),
        t({"", "}"}),
    }),

    -- full if statement
    s("fif",
    {
        t("if ("),
        i(1, "condition"),
        t(") {"),
        t({"", "\t"}),
        i(2, ""),
        t({"", "} else if ("}),
        i(3, "condition 2"),
        t(") {"),
        t({"", "\t"}),
        i(4, ""),
        t({"", "} else {"}),
        t({"", "\t"}),
        i(0),
        t({"", "}"}),
    }),

    -- if-else
    s("ife",
    {
        t("if ("),
        i(1, "condition"),
        t(") {"),
        t({"", "\t"}),
        i(2, ""),
        t({"", "} else {"}),
        t({"", "\t"}),
        i(0, ""),
        t({"", "}"}),
    }),

    s("stc",
    {
        t("typedef struct "),
        -- copy the text from i(1) and append it to 'struct '
        f(copy, 1),
        t({ " {", "\t" }),
        i(0),
        t({"", "} "}),
        i(1),
        t(";"),
    }),

    s("sfr",
    {
        t("for (int i = 0; i < "),
        i(1, "x"),
        t("; i++) {"),
        t({"", "\t"}),
        i(0),
        t({"", "}"}),
    }),

    s("fr",
    {
        t("for (int "),
        i(1, "x"),
        t(" = "),
        i(2),
        t("; "),
        f(copy, 1),
        t(" < "),
        i(3),
        t("; "),
        f(copy, 1),
        t("++) {"),
        t({"", "\t"}),
        i(0),
        t({"", "}"}),
    }),

    s("inc",
    {
        t("#include <"),
        i(0),
        t(".h>"),
    }),

    s("incs",
    {
        t("#include \""),
        i(0),
        t(".h\""),
    }),

    s("pr",
    {
        t("printf(\""),
        i(0),
        t("\");"),
    }),

    s("prs",
    {
        t("printf(\"%s"),
        t(""),
        i(0),
        t("\", "),
        i(1),
        t(");"),
    }),

    s("mlc",
    {
        i(1, "type"),
        t("* "),
        i(2, "name"),
        t(" = ("),
        f(copy, 1),
        t("*)malloc(sizeof("),
        f(copy, 1),
        t(")"),
        i(0),
        t(");"),
    }),

}
