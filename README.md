# Dotfiles
Currently in use:

|  |  |
| ------ | ------ |
| **WM** | Dwm |
| **hotkeys** | Sxhkd |
| **terminal** | st |
| **editor** | neovim |
| **bar** | Slstatus |
| **notifications** | Dunst |
| **launcher** | Dmenu |
