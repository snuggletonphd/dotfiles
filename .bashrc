# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
#
#===================================================================================================
export PATH="$PATH:$(du "$HOME/.scripts" | cut -f2 | paste -sd ':')"
#
export TERMINAL="st"
export EDITOR="nvim"
export BROWSER="firefox"
export READER="zathura"
export NVIMRC="$HOME/.config/nvim/init.lua"
source ~/.config/nnn/nnn.bash
#

PS1=" \[\e[1;36m\]>-{\[\e[0;37m\]\u\[\e[1;36m\]|\[\e[0;37m\]\h\[\e[1;36m\]}-{\[\e[0;37m\]\w\[\e[1;36m\]}-> \[\e[00m\]"
#===================================================================================================
#
#
#======================
#==========================
# ALIASES #=====================
#==========================
#======================
#
#
#======================
# SYSTEM #================
#======================
#
alias Si='sudo xbps-install'
alias Se='sudo xbps-query -Rs'
alias Sr='sudo xbps-remove'
alias s='sudo'
alias pacman='sudo pacman'
alias x='exit'
#
#======================
# NAVIGATION #============
#======================
#
alias c='cd ..'
alias d='cd'
alias dn='cd ~/.config/nvim'
alias dc='cd ~/code/c'
alias dm='cd ~/.music'
alias dcg='cd ~/code/c/garbage'
alias cfg='cd ~/.config'
alias nh='cd ~/.config/nvim/hotkeys'
alias dl='cd ~/Downloads'
alias dp='cd ~/Pictures'
alias dt='cd ~/git/dotfiles'
alias dg='cd ~/git'
alias dgt='cd ~/git/dotfilestemp'
alias d4='cd ~/Pictures/4chan'
alias tdl='cd ~/.local/share/tor-browser_en-US/Browser/Downloads'
alias scb='source ~/.bashrc'
alias scp='cd $HOME/.scripts'
#
#======================
# DISPLAY #===============
#======================
#
alias ll='ls -lh'
alias lla='ls -lah'
alias la='ls -A'
alias l='ls -CF'
alias h='history'
alias j='jobs -l'
alias df='df -h'
#
# list processes #------
alias psf='ps -aux'
alias psa='ps -a'
alias psu='ps -u'
alias psx='ps -x'
alias PSF='ps -aux | less'
alias PSA='ps -a | less'
alias PSU='ps -u | less'
alias PSX='ps -x | less'
#
#======================
# CREATION #==============
#======================
#
alias dr='mkdir -pv'
alias DR='sudo mkdir -pv'
alias t='touch'
alias T='sudo touch'
#
#=======================
# MODIFICATION #============
#=======================
#
alias cr='cp -r'
alias CR='sudo cp -r'
alias CP='sudo cp'
alias m='mv'
alias M='sudo mv'
#
#======================
# DELETION #==============
#======================
#
alias r='rm'
alias R='sudo rm'
alias rd='rmdir'
alias RD='sudo rmdir'
alias rr='rm -r -I'
alias RR='sudo rm -r -I'
#
#======================
# VIM #===================
#======================
#
alias v='nvim'
alias vb='nvim ~/.bashrc'
alias vn='nvim $NVIMRC'
alias V='sudo nvim'
alias F='nvim $(fzf --walker-root=$HOME)'
alias vm='nvim src/main.*'
alias VXKB='sudo nvim /usr/share/X11/xkb/symbols/dvpc'
#
#======================
# PROGRAMS #==============
#======================
#
alias nf='neofetch'
alias untar='tar xvf'
alias UNTAR='sudo tar xvf'
alias diff='colordiff'
alias f='devour feh'
alias ff='feh -F'
alias dmpv='devour mpv'
alias gif='sxiv -a'
alias xpdf='devour xpdf'
alias z='zathura'
alias dz='devour zathura'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#
#===================================
# PERMISSIONS & OWNERSHIP #============
#===================================
#
alias run='chmod +x'
alias RUN='sudo chmod +x'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
#
#===========================
# GIT #========================
#===========================
#
alias gs='git status'
alias ga='git add'
alias gr='git rm'
alias gc='git clone'
alias gm='git commit -m'
alias gp='git push -u'
alias gd='git diff'
#
#===========================
# MISCELLANEOUS #==============
#===========================
#
alias mr='make run'
alias yta='youtube-dl -i -x -f bestaudio/best --add-metadata'
alias calc='bc -l'
#
#===========================
# FUNCTIONS #==================
#===========================
#
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "n³ is already running"
        return
    fi

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    nnn -Rd "$@"

    if [ -f "$NNN_TMPFILE" ]; then
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    fi
}
#
#===========================
# TEMP #=======================
#===========================
#
alias rk='./bin/kek bin/k'
#
#===================================================================================================
